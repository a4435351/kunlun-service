package com.kunlun.basedata.controller;

import com.kunlun.basedata.model.vo.AllotAuthorizeVo;
import com.kunlun.basedata.model.vo.AuthorizeDetailVo;
import com.kunlun.basedata.service.IAllotAuthorizeService;
import com.kunlun.common.model.Page;
import com.kunlun.common.utils.ResponseUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/authorize")
public class AuthorizeController {

    private Logger log = LogManager.getLogger();

    @Autowired
    private IAllotAuthorizeService allotAuthorizeService;

    @RequestMapping(value = "/getAllotDepartments", method = RequestMethod.GET)
    public Object getAllotDepartments(AllotAuthorizeVo allotAuthorizeVo, int currentPage, int pageSize) {
        try {
            Page list = allotAuthorizeService.getAllotDepartments(allotAuthorizeVo, currentPage, pageSize);
            return ResponseUtil.successResponse(list);
        } catch (Exception e) {
            log.error("AuthorizeController getAllotDepartments Error: ", e);
            return ResponseUtil.failedResponse("查询部门人员分派失败！", e.getMessage());
        }
    }

    @RequestMapping(value = "/getAllotPosts", method = RequestMethod.GET)
    public Object getAllotPosts(AllotAuthorizeVo allotAuthorizeVo, int currentPage, int pageSize) {
        try {
            Page list = allotAuthorizeService.getAllotPosts(allotAuthorizeVo, currentPage, pageSize);
            return ResponseUtil.successResponse(list);
        } catch (Exception e) {
            log.error("AuthorizeController addAllotCorrelate Error: ", e);
            return ResponseUtil.failedResponse("查询岗位人员分派失败！", e.getMessage());
        }
    }

    @RequestMapping(value = "/getAllotRoles", method = RequestMethod.GET)
    public Object getAllotRoles(AllotAuthorizeVo allotAuthorizeVo, int currentPage, int pageSize) {
        try {
            Page list = allotAuthorizeService.getAllotRoles(allotAuthorizeVo, currentPage, pageSize);
            return ResponseUtil.successResponse(list);
        } catch (Exception e) {
            log.error("AuthorizeController addAllotCorrelate Error: ", e);
            return ResponseUtil.failedResponse("查询角色人员分派失败！", e.getMessage());
        }
    }

    @RequestMapping(value = "/addAllotCorrelate", method = RequestMethod.POST)
    public Object addAllotCorrelate(String allotId, String type, String selectedKeys) {
        try {
            String[] splits = selectedKeys.split(",");
            allotAuthorizeService.addAllotCorrelate(allotId, type, splits);
            return ResponseUtil.successResponse("关联授权成功");
        } catch (Exception e) {
            log.error("AuthorizeController addAllotCorrelate Error: ", e);
            return ResponseUtil.failedResponse("关联授权失败！", e.getMessage());
        }
    }

    @RequestMapping(value = "/getAllotAuthorizeDetail", method = RequestMethod.GET)
    public Object getAllotAuthorizeDetail(String allotId, String correlateId, String type) {
        try {
            AuthorizeDetailVo authorizeDetailVo = allotAuthorizeService.getAllotAuthorizeDetail(allotId, correlateId, type);
            return ResponseUtil.successResponse(authorizeDetailVo);
        } catch (Exception e) {
            log.error("AuthorizeController addAllotCorrelate Error: ", e);
            return ResponseUtil.failedResponse("查询关联权限详情失败！", e.getMessage());
        }
    }

    @RequestMapping(value = "/deleteAllots", method = RequestMethod.GET)
    public Object deleteAllots(String ids) {
        try {
            allotAuthorizeService.deleteAllots(ids);
            return ResponseUtil.successResponse("删除成功");
        } catch (Exception e) {
            log.error("AuthorizeController addAllotCorrelate Error: ", e);
            return ResponseUtil.failedResponse("删除失败！", e.getMessage());
        }
    }
}
