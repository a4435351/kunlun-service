package com.kunlun.basedata.service;

import com.kunlun.basedata.model.vo.AllotAuthorizeVo;
import com.kunlun.basedata.model.vo.AuthorizeDetailVo;
import com.kunlun.common.model.Page;

/**
 * 岗位业务接口
 */
public interface IAllotAuthorizeService {

    public Page<AllotAuthorizeVo> getAllotDepartments(AllotAuthorizeVo allotAuthorizeVo, int currentPage, int pageSize) throws Exception;

    public Page<AllotAuthorizeVo> getAllotPosts(AllotAuthorizeVo allotAuthorizeVo, int currentPage, int pageSize) throws Exception;

    public Page<AllotAuthorizeVo> getAllotRoles(AllotAuthorizeVo allotAuthorizeVo, int currentPage, int pageSize) throws Exception;

    public void addAllotCorrelate(String allotId, String type, String[] selectedKeys) throws Exception;

    public AuthorizeDetailVo getAllotAuthorizeDetail(String allotId, String correlateId, String type) throws Exception;

    public void deleteAllots(String ids) throws Exception;
}
