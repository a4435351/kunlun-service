package com.kunlun.basedata.model.vo;

import com.kunlun.basedata.model.MenuModel;
import com.kunlun.basedata.model.UserModel;

import java.util.List;

/**
 * 关联授权详情展示VO
 */
public class AuthorizeDetailVo {

    private String id;

    private String allotId;

    private String type;

    private String correlateName;

    private String operateTime;

    private String userId;

    private String userName;

    private String sex;

    private String phoneNumber;

    private String email;

    private List<MenuModel> menuModels;

    public AuthorizeDetailVo() {}

    public AuthorizeDetailVo(AllotAuthorizeVo allotAuthorizeVo, UserModel userModel) {
        this.id = allotAuthorizeVo.getId();
        this.allotId = allotAuthorizeVo.getAllotId();
        this.type = allotAuthorizeVo.getType();
        this.correlateName = allotAuthorizeVo.getCorrelateName();
        this.operateTime = allotAuthorizeVo.getOperateTime();
        this.userId = userModel.getId();
        this.userName = userModel.getUserName();
        this.sex = userModel.getSex();
        this.email = userModel.getEmail();
        this.phoneNumber = userModel.getPhoneNumber();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAllotId() {
        return allotId;
    }

    public void setAllotId(String allotId) {
        this.allotId = allotId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCorrelateName() {
        return correlateName;
    }

    public void setCorrelateName(String correlateName) {
        this.correlateName = correlateName;
    }

    public String getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(String operateTime) {
        this.operateTime = operateTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<MenuModel> getMenuModels() {
        return menuModels;
    }

    public void setMenuModels(List<MenuModel> menuModels) {
        this.menuModels = menuModels;
    }
}
