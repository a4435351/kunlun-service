package com.kunlun.basedata.model;

import com.kunlun.common.model.BaseModel;

/**
 * 人员分配菜单授权模型
 */
public class AllotModel extends BaseModel {

    /**
     * 分配id（部门id、岗位id或角色id）
     */
    private String allotId;

    /**
     * 关联id（用户id）
     */
    private String correlateId;

    /**
     * 权限id（菜单id）
     */
    private String authorizeId;

    /**
     * 关联类型：1-部门人员分派；2-岗位人员任命；3-岗位菜单授权；4-角色关联用户；5-角色菜单授权；
     */
    private String type;

    public String getAllotId() {
        return allotId;
    }

    public void setAllotId(String allotId) {
        this.allotId = allotId;
    }

    public String getCorrelateId() {
        return correlateId;
    }

    public void setCorrelateId(String correlateId) {
        this.correlateId = correlateId;
    }

    public String getAuthorizeId() {
        return authorizeId;
    }

    public void setAuthorizeId(String authorizeId) {
        this.authorizeId = authorizeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
