package com.kunlun.basedata.model;

import com.kunlun.common.model.BaseModel;

/**
 * 角色模型
 */
public class RoleModel extends BaseModel {

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色字符
     */
    private String roleWord;

    /**
     * 状态
     */
    private boolean status;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleWord() {
        return roleWord;
    }

    public void setRoleWord(String roleWord) {
        this.roleWord = roleWord;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
