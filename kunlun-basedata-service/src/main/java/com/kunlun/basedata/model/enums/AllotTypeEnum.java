package com.kunlun.basedata.model.enums;

/**
 * 分配类型枚举
 */
public enum AllotTypeEnum {

    DEPARTMENT("department", "部门"),

    POST("post", "岗位"),

    ROLE("role", "角色");

    private String key;

    private String desc;

    AllotTypeEnum(String key, String desc) {
        this.key = key;
        this.desc = desc;
    }

    public String getKey() {
        return key;
    }

    public String getDesc() {
        return desc;
    }
}
