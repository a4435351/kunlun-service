package com.kunlun.basedata.model.vo;

/**
 * 关联授权页面展示VO
 */
public class AllotAuthorizeVo {

    private String id;

    private String userId;

    private String userName;

    private String correlateId;

    private String correlateName;

    private String allotId;

    private String type;

    private String authorizeId;

    private String authorizeItem;

    private String operateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCorrelateId() {
        return correlateId;
    }

    public void setCorrelateId(String correlateId) {
        this.correlateId = correlateId;
    }

    public String getCorrelateName() {
        return correlateName;
    }

    public void setCorrelateName(String correlateName) {
        this.correlateName = correlateName;
    }

    public String getAllotId() {
        return allotId;
    }

    public void setAllotId(String allotId) {
        this.allotId = allotId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAuthorizeId() {
        return authorizeId;
    }

    public void setAuthorizeId(String authorizeId) {
        this.authorizeId = authorizeId;
    }

    public String getAuthorizeItem() {
        return authorizeItem;
    }

    public void setAuthorizeItem(String authorizeItem) {
        this.authorizeItem = authorizeItem;
    }

    public String getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(String operateTime) {
        this.operateTime = operateTime;
    }
}
