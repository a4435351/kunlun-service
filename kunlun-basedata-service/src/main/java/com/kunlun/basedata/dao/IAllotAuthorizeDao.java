package com.kunlun.basedata.dao;

import com.kunlun.basedata.model.AllotModel;
import com.kunlun.basedata.model.vo.AllotAuthorizeVo;
import com.kunlun.basedata.model.vo.AuthorizeDetailVo;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 岗位数据库Dao
 */
@Repository
public interface IAllotAuthorizeDao {

    public List<AllotAuthorizeVo> getAllotDepartments(Map<String, Object> queryMap) throws Exception;

    public int getAllotDepartmentCount(Map<String, Object> queryMap) throws Exception;

    public List<AllotAuthorizeVo> getAllotPosts(Map<String, Object> queryMap) throws Exception;

    public int getAllotPostCount(Map<String, Object> queryMap) throws Exception;

    public List<AllotAuthorizeVo> getAllotRoles(Map<String, Object> queryMap) throws Exception;

    public int getAllotRoleCount(Map<String, Object> queryMap) throws Exception;

    public int addAllotCorrelate(List<AllotModel> list) throws Exception;

    public AllotAuthorizeVo getAuthorizeItem(@Param("allotId") String allotId, @Param("type") String type) throws Exception;

    public void deleteAllots(List<String> ids) throws Exception;
}
