package com.kunlun.basedata;

import com.kunlun.basedata.task.ScheduleTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Integer.MAX_VALUE - 8)
public class CustomApplicationRunner implements ApplicationRunner {

    @Autowired
    private ScheduleTaskService scheduleTaskService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        // 定时删除缓存的Token
        scheduleTaskService.prepareCache();
    }
}
