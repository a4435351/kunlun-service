package com.kunlun.common.listener;

import com.kunlun.common.constant.CommonConstant;
import com.kunlun.common.utils.SchemaUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;

/**
 * SpringBoot服务运行公共监听器
 */
public class CommonRunListener implements SpringApplicationRunListener, Ordered {

    private static Logger logger = LogManager.getLogger();

    public CommonRunListener(SpringApplication application, String[] args) {
        logger.info("========== CommonRunListener ========== ");
    }

    @Override
    public void starting() {
        logger.info("========== CommonRunListener starting ==========");
    }

    @Override
    public void environmentPrepared(ConfigurableEnvironment environment) {
        logger.info("========== CommonRunListener environmentPrepared ==========");
    }

    @Override
    public void contextPrepared(ConfigurableApplicationContext context) {
        logger.info("========== CommonRunListener contextPrepared ==========");
    }

    @Override
    public void contextLoaded(ConfigurableApplicationContext context) {
        logger.info("========== CommonRunListener contextLoaded ==========");
        Environment environment = context.getEnvironment();
        if (null != environment) {
            // 服务名
            String serviceName = environment.getProperty(CommonConstant.APPLICATON_NAME_PATH);
            logger.info("========== CommonRunListener contextLoaded serviceName ===>>> " + serviceName);

            // 初始化数据库
            String rootPath = System.getProperty("user.dir");
            if ("kunlun-basedata-service".equals(serviceName)) {
                SchemaUtil.checkAndInitSchema("kunlun_home", serviceName, rootPath, "/BaseDataSQL.sql");
            } else if ("kunlun-system-service".equals(serviceName)) {
                SchemaUtil.checkAndInitSchema("kunlun_system", serviceName, rootPath, "/SystemSQL.sql");
                SchemaUtil.checkAndInitSchema("kunlun_activiti", serviceName, rootPath, "/ActivitiSQL.sql");
            }
        }
    }

    @Override
    public void started(ConfigurableApplicationContext context) {
        logger.info("========== CommonRunListener started ==========");
    }

    @Override
    public void running(ConfigurableApplicationContext context) {
        logger.info("========== CommonRunListener running ==========");
    }

    @Override
    public void failed(ConfigurableApplicationContext context, Throwable exception) {
        logger.info("========== CommonRunListener failed ==========");
    }

    @Override
    public int getOrder() {
        logger.info("========== CommonRunListener getOrder ==========");
        return 0;
    }
}
