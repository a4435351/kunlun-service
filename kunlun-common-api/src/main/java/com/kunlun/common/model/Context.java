package com.kunlun.common.model;

import org.springframework.http.server.ServerHttpRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * 上下文缓存
 */
public class Context {

    /**
     * 当前登录账号
     */
    private static ThreadLocal<CurrentAccount> currentAccount = new InheritableThreadLocal<>();

    /**
     * 当前HttpRequest
     */
    private static ThreadLocal<HttpServletRequest> httpRequest = new InheritableThreadLocal<>();

    public static CurrentAccount getCurrentAccount() {
        return currentAccount.get();
    }

    public static void setCurrentAccount(CurrentAccount current) {
        currentAccount.set(current);
    }

    public static void clearCurrentAccount() {
        currentAccount.remove();
    }

    public static HttpServletRequest getHttpRequest() {
        return httpRequest.get();
    }

    public static void setHttpRequest(HttpServletRequest request) {
        httpRequest.set(request);
    }

    public static void clearHttpRequest() {
        httpRequest.remove();
    }
}
