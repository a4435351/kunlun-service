package com.kunlun.common.model.monitor;

/**
 * CPU信息模型
 */
public class CpuInfoModel {

    /**
     * 物理处理器数量
     */
    private int physicalProcessorCount;

    /**
     * 逻辑处理器数量
     */
    private int logicalProcessorCount;

    /**
     * 系统使用率
     */
    private double systemPercent;

    /**
     * 用户使用率
     */
    private double userPercent;

    /**
     * 当前等待率
     */
    private double waitPercent;

    /**
     * 当前使用率
     */
    private double usePercent;

    public int getPhysicalProcessorCount() {
        return physicalProcessorCount;
    }

    public void setPhysicalProcessorCount(int physicalProcessorCount) {
        this.physicalProcessorCount = physicalProcessorCount;
    }

    public int getLogicalProcessorCount() {
        return logicalProcessorCount;
    }

    public void setLogicalProcessorCount(int logicalProcessorCount) {
        this.logicalProcessorCount = logicalProcessorCount;
    }

    public double getSystemPercent() {
        return systemPercent;
    }

    public void setSystemPercent(double systemPercent) {
        this.systemPercent = systemPercent;
    }

    public double getUserPercent() {
        return userPercent;
    }

    public void setUserPercent(double userPercent) {
        this.userPercent = userPercent;
    }

    public double getWaitPercent() {
        return waitPercent;
    }

    public void setWaitPercent(double waitPercent) {
        this.waitPercent = waitPercent;
    }

    public double getUsePercent() {
        return usePercent;
    }

    public void setUsePercent(double usePercent) {
        this.usePercent = usePercent;
    }
}
