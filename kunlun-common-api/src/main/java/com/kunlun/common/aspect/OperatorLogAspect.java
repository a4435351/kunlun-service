package com.kunlun.common.aspect;

import com.alibaba.fastjson.JSONObject;
import com.kunlun.common.amqp.OperatorLogSender;
import com.kunlun.common.annotation.OperatorLogger;
import com.kunlun.common.model.Context;
import com.kunlun.common.model.OperatorLogModel;
import com.kunlun.common.utils.CommonUtil;
import com.kunlun.common.utils.JwtTokenUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * 操作日志记录AOP类
 */
@Component
@Aspect
public class OperatorLogAspect {

    /**
     * 操作日志业务Id
     */
    private String logId = "";

    private Logger log = LogManager.getLogger();

    @Autowired
    private OperatorLogSender operatorLogSender;

    @Value("${spring.application.name}")
    private String serviceName;

    @Value("${server.port}")
    private int port;

    @Pointcut("@annotation(com.kunlun.common.annotation.OperatorLogger)")
    public void pointCut() {}

    @Before("pointCut()")
    public void logBefore(JoinPoint joinPoint) throws Throwable {
        // 获取http请求信息
        HttpServletRequest request = Context.getHttpRequest();
        StringBuffer requestUrl = request.getRequestURL();
        String style = request.getMethod();
        String protocal = request.getProtocol();
        String params = request.getQueryString();
        String ip = CommonUtil.getIpAddr(request);
        log.info("requestUrl ===>>> " + requestUrl + "， style ===>>> " + style + "， protocal ===>>> " + protocal + "， params ===>>> " + params + "， ip ===>>> " + ip);

        // 获取执行的类及方法
        String clzType = joinPoint.getTarget().getClass().getName();
        Class<?> clz = Class.forName(clzType);
        String clzName = clz.getName();
        String methodName = joinPoint.getSignature().getName();

        // 获取注解及其内容
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        OperatorLogger annotation = method.getAnnotation(OperatorLogger.class);
        int operatorType = annotation.type().getKey();
        String description = annotation.description();
        log.info("clzName ===>>> " + clzName + "， methodName ===>>> " + methodName + "， operatorType ===>>> " + operatorType + "， description ===>>> " + description);

        // 生成操作日志业务Id，保证每个OperatorLogger注解的logId唯一，即一个OperatorLogger对应一个logId
        logId = CommonUtil.generateUUID();

        // 发送操作日志记录消息
        String userName = extractUserName(request, params);
        OperatorLogModel logModel = buildOperatorLogModel(requestUrl, style, protocal, params, ip, clzName, methodName, operatorType, description, userName);
        String logJson = JSONObject.toJSONString(logModel);
        operatorLogSender.sendOperatorLog(logJson);
    }

    private String extractUserName(HttpServletRequest request, String params) {
        String userName = "";
        if (!ObjectUtils.isEmpty(params) && params.contains("userName")) {
            String[] paramArray = params.split("=");
            int index = 0;
            for (int i = 0; i < paramArray.length; i++) {
                if (paramArray[i].contains("userName")) {
                    index = i;
                }
            }
            String userNameStr = params.split("=")[index + 1];
            userName = userNameStr.indexOf("&") > -1 ? userNameStr.substring(0, userNameStr.indexOf("&")) : userNameStr;
        } else {
            String token = request.getHeader("Authorization");
            userName = JwtTokenUtil.getTokenInfo(token, "userName");
        }
        return userName;
    }

    private OperatorLogModel buildOperatorLogModel(StringBuffer requestUrl, String style, String protocal, String params, String ip, String clzName, String methodName, int operatorType, String description, String userName) {
        OperatorLogModel logModel = new OperatorLogModel();
        logModel.setId(logId);
        logModel.setIp(ip);
        logModel.setUserName(userName);
        logModel.setOperatorType(operatorType);
        logModel.setOperateDescription(description);
        logModel.setRequestUrl(requestUrl.toString());
        logModel.setProtocal(protocal);
        logModel.setParams(params);
        logModel.setClzName(clzName);
        logModel.setMethodName(methodName);
        logModel.setOperateTime(new Date());
        logModel.setStyle(style);
        logModel.setServiceName(serviceName);
        logModel.setPort(port);
        logModel.setThreadName(Thread.currentThread().getName());
        logModel.setStatus("正常");
        return logModel;
    }

    @AfterThrowing(pointcut = "pointCut()", throwing = "e")
    public void logException(JoinPoint joinPoint, Exception e) throws Throwable {
        // 获取执行的类及方法
        String clzType = joinPoint.getTarget().getClass().getName();
        Class<?> clz = Class.forName(clzType);
        String clzName = clz.getName();
        String methodName = joinPoint.getSignature().getName();
        log.info("clzName ===>>> " + clzName + ", methodName ===>>> " + methodName + ", exception ===>>> " + e.getMessage());

        // 创建操作日志模型
        OperatorLogModel logModel = new OperatorLogModel();
        logModel.setId(logId);
        logModel.setExceptionInfo(e.getMessage());
        logModel.setStatus("异常");

        // 发送操作日志记录消息
        String logJson = JSONObject.toJSONString(logModel);
        operatorLogSender.sendOperatorLog(logJson);
    }
}
