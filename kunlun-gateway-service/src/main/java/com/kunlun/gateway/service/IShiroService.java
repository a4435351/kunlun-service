package com.kunlun.gateway.service;

import com.kunlun.gateway.model.TokenModel;

public interface IShiroService {

    public TokenModel handleLogin(String userName, String password) throws Exception;

    public void handleLogout(String userName) throws Exception;
}
