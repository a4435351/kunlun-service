package com.kunlun.gateway.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

@Component
@Primary
public class DocumentConfig implements SwaggerResourcesProvider {

    private Logger log = LogManager.getLogger();

    private static final String API_URI = "/v2/api-docs";

    @Override
    public List<SwaggerResource> get() {
        log.info("===== Swagger2 Documentation Config =====");
        List<SwaggerResource> resources = new ArrayList<>();
        resources.add(createResource("基础数据服务", "/kunlun-basedata-service/v2/api-docs", "2.0"));
        resources.add(createResource("系统业务服务", "/kunlun-system-service/v2/api-docs", "2.0"));
        return resources;
    }

    private SwaggerResource createResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}
